<?php
    declare(strict_types=1);

    namespace Demo;

    use Demo\Adapters\AuthenticatedRpcAdapter;
    use Demo\Adapters\ComplexRpcAdapter;
    use Demo\Adapters\DependencyInjectedRpcAdapter;
    use Demo\Adapters\EnrichedRequestRpcAdapter;
    use Demo\Adapters\GetRpcAdapter;
    use Demo\Adapters\HelloEndpointAdapter;
    use Demo\Adapters\OtherAuthenticatedRpcAdapter;
    use Demo\Dal\SomeDataAccess;
    use Demo\Rpc\DependencyInjectedRpc;
    use Demo\Rpc\OtherAuthenticatedRpc;
    use Laminas\Diactoros\ServerRequestFactory;
    use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
    use Proresult\PhpTypescriptRpc\Server\AllowedCorsOrigin;
    use Proresult\PhpTypescriptRpc\Server\CorsProcessor;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\RpcRouter;

    require __DIR__.'/../../vendor/autoload.php';

    function dependencyInjectedRpc(): DependencyInjectedRpc {
        // Here we pre-create a instance of the DependencyInjectedRpc class to demo how one can do runtime dependency injection and override the default initFromRequest function on
        // the rpc class.
        $someData = new SomeDataAccess(); // The dependency to inject
        return new DependencyInjectedRpc($someData);
    }

    function main(): void {
        $responseFactory = new ResponseFactory(); // A factory of PSR-7 response instances. This demo uses the Laminas\Diactoras library for this, but one is free to use any other compliant implementation.
        $requestResponseUtils = new RequestResponseUtils($responseFactory);

        // The rpc adapter classes are initialized here. This let's us do dependency injection into rpc classes, or other setup if we want.
        // Should not do anything heavy here, since it is done on each request no matter what Rpc class the request is intended for.
        // Inject/use factories to avoid doing unnecessary work before a request is actually matched to the rpc class.
        $helloClass = new HelloEndpointAdapter($requestResponseUtils);
        $complexRpcAdapter = new ComplexRpcAdapter($requestResponseUtils);
        $dependencyInjectedRpcAdapter = new DependencyInjectedRpcAdapter($requestResponseUtils, \Demo\dependencyInjectedRpc() /* Pass in a precreated rpc class instance (prevents the initFromRequest to be run) */);
        $getRpcAdapter = new GetRpcAdapter($requestResponseUtils);
        $enrichedRpcAdapter = new EnrichedRequestRpcAdapter($requestResponseUtils);
        $authenticatedRpcAdapter = new AuthenticatedRpcAdapter($requestResponseUtils);
        $otherAuthenticatedRpcAdapter = new OtherAuthenticatedRpcAdapter($requestResponseUtils);

        // If the rpc server is to be usable from web pages served from a different origin, we need to setup a CORS processor.
        $allowedCorsOrigins = [new AllowedCorsOrigin("http", "localhost", 8000)];
        $corsProcessor = new CorsProcessor(["GET", "POST"], $allowedCorsOrigins, $responseFactory, true);

        // Initialize the RpcRouter which hooks all the rpc adapter classes into the request response handling.
        // Here we set a basepath for all rpcs. If set, the same basepath must be passed when initializing the rpc Client class in the browser, so that it knows to add this basepath to all request urls.
        $handler = new RpcRouter($responseFactory, "/rpc", $helloClass, $complexRpcAdapter, $dependencyInjectedRpcAdapter, $getRpcAdapter, $enrichedRpcAdapter, $authenticatedRpcAdapter, $otherAuthenticatedRpcAdapter);
        $handler->setCorsProcessor($corsProcessor);

        // Handle incoming http request.
        $request = ServerRequestFactory::fromGlobals();
        $response = $handler->process($request);

        // Write the response back to the calling client.
        $emitter = new SapiEmitter();
        $emitter->emit($response);
    }

    main();