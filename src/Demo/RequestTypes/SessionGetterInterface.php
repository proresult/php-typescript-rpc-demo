<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\RequestTypes;

    use Demo\Models\Session;

    interface SessionGetterInterface {
        public function session(): Session;
    }