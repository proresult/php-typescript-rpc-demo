<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\RequestTypes;

    use Demo\Models\Session;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;

    trait RequestSessionTrait {

        private ?Session $session = null;

        abstract public function getSessionSecret(): string;

        public function session(): Session {
            if($this->session === null) {
                $this->session = Session::fromAuthHeader($this->getSessionSecret());
            }
            if($this->session === null) {
                throw new UnauthorizedException("no valid session found in request header");
            }
            return $this->session;
        }
    }