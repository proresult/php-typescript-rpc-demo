<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\RequestTypes;

    class SessionRequest extends EnrichedRequest implements SessionGetterInterface {
        use RequestSessionTrait;
    }