<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\RequestTypes;

    use phpDocumentor\Reflection\DocBlock\Tags\Formatter\PassthroughFormatter;
    use Proresult\PhpTypescriptRpc\Server\WrappedRequest;
    use Psr\Http\Message\ServerRequestInterface;

    class EnrichedRequest extends WrappedRequest {
        public const HEADER_KEY_REQUEST_ID = "X-Request-Id";
        public const HEADER_KEY_SESSION_SECRET = "X-Auth-Secret";
        public const HEADER_KEY_CLIENT_VERSION = "X-Client-Version";

        private int $requestId;
        private string $sessionSecret;
        private ?string $clientVersion;

        private static function firstHeaderValue(ServerRequestInterface $request, string $key): ?string {
            foreach ($request->getHeader($key) as $header) {
                return $header;
            }
            return null;
        }

        public static function requestIdFromHeader(ServerRequestInterface $request): int {
            $id = self::firstHeaderValue($request, self::HEADER_KEY_REQUEST_ID);
            if (is_numeric($id)) {
                return (int)$id;
            }
            return 0;
        }

        public static function sessionSecretFromHeader(ServerRequestInterface $request): string {
            return self::firstHeaderValue($request, self::HEADER_KEY_SESSION_SECRET) ?? "";
        }

        public static function clientVersionFromHeader(ServerRequestInterface $request): ?string {
            return self::firstHeaderValue($request, self::HEADER_KEY_CLIENT_VERSION);
        }

        public function __construct(ServerRequestInterface $request) {
            $this->requestId = self::requestIdFromHeader($request);
            $this->sessionSecret = self::sessionSecretFromHeader($request);
            $this->clientVersion = self::clientVersionFromHeader($request);
            parent::__construct($request);
        }

        public function getRequestId(): int {
            return $this->requestId;
        }

        public function getSessionSecret(): string {
            return $this->sessionSecret;
        }

        public function getClientVersion(): ?string {
            return $this->clientVersion;
        }
    }