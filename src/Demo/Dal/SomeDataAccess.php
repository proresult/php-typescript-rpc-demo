<?php
    declare(strict_types=1);

    namespace Demo\Dal;


    /**
     * Class SomeDataAccess pretends to be a useful class accessing a database or something to get the data for rpc responses.
     *
     * @package Demo\Dal
     */
    class SomeDataAccess implements SomeDataAccessInterface {
        function getPersonAge(int $personId) : int {
            return $personId * 10 + 3;
        }
    }