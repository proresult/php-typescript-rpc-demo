<?php
    declare(strict_types=1);

    namespace Demo\Dal;


    interface SomeDataAccessInterface {
        function getPersonAge(int $personId): int;
    }