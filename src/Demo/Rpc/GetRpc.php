<?php
    declare(strict_types=1);

    namespace Demo\Rpc;

    use Demo\Models\CircularRef;
    use Demo\Models\GetSomeResponse;
    use Demo\Models\TrueOrFalseResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Psr\Http\Message\ServerRequestInterface;

    class GetRpc {

        static function initFromRequest(ServerRequestInterface $request): GetRpc {
            return new GetRpc();
        }

        #[Get]
        function getSome(string $txt1, int $num1, string $txt2 = "some default value"): GetSomeResponse {
            return new GetSomeResponse($txt1, $num1, $txt2);
        }

        #[Get]
        function trueOrFalse(bool $answer, float $bonus): TrueOrFalseResponse {
            return new TrueOrFalseResponse($answer, $bonus*2);
        }

        #[Get]
        public function nullableArguments(?string $maybeTxt1, int | null $maybeNum1, ?string $defaultTxt2 = "defaulted2"): GetSomeResponse {
            return new GetSomeResponse($maybeTxt1 ?? "was null", $maybeNum1 ?? -99, $defaultTxt2 ?? "not defaulted");
        }

        #[Get]
        #[Retryable(0)]
        public function getCircularRef(): CircularRef {
            $innerRefs = [
                new CircularRef(new CircularRef(null, -11), -1),
                new CircularRef(null, -2),
                new CircularRef(new CircularRef(null, -33), -3)
            ];
            return new CircularRef(new CircularRef(new CircularRef(null, 4444), 333), 22, $innerRefs);
        }
    }