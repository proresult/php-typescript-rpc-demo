<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Rpc;

    use DateInterval;
    use Demo\AuthControl\RequestAuthControl;
    use Demo\Models\HelloRequest;
    use Demo\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;
    use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Psr\Http\Message\ServerRequestInterface;

    /**
     * Demoes a rpc class with AuthType, but without a separate RequestType
     */
    #[AuthType(RequestAuthControl::class)]
    class OtherAuthenticatedRpc {

        public static function initFromRequest(ServerRequestInterface $request): OtherAuthenticatedRpc {
            return new self();
        }

        #[Retryable(4)]
        #[AuthCheck("hello", true, 'helloRequest->name')]
        public function hello(HelloRequest $helloRequest): HelloResponse {
            return new HelloResponse("Hi, {$helloRequest->name}", $helloRequest->today->add(new DateInterval("P1D")));
        }
    }