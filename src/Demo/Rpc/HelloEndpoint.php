<?php
    declare(strict_types=1);

    namespace Demo\Rpc;


    use DateTimeImmutable;
    use Demo\Models\HelloRequest;
    use Demo\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
    use Psr\Http\Message\ServerRequestInterface;

    class HelloEndpoint {

        public function __construct(private DateTimeImmutable $startTime) {
        }

        #[Retryable]
        public function hello(HelloRequest $request): HelloResponse {
            $response = new HelloResponse(
                "Hello, {$request->name}!",
                new RpcDateTime($request->today->add(new \DateInterval("P1D"))),
            );
            return $response;
        }

        public function failingHello(HelloRequest $request): HelloResponse {
            throw new UserfriendlyException("I'm failing on purpose", 1010);
        }

        static function initFromRequest(ServerRequestInterface $request): HelloEndpoint {
            return new HelloEndpoint(new DateTimeImmutable());
        }
    }