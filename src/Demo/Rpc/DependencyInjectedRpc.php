<?php
    declare(strict_types=1);

    namespace Demo\Rpc;


    use Demo\Dal\SomeDataAccess;
    use Demo\Dal\SomeDataAccessInterface;
    use Demo\Models\PersonInfoRequest;
    use Demo\Models\PersonInfoResponse;
    use Psr\Http\Message\ServerRequestInterface;

    /**
     * Class DependencyInjectedRpc Show how one can create rpc classes that do dependency injection for unit testing.
     *
     * This one takes in a instance of a data access interface. When used for real it creates the real data access interface itself. When used in unit testing, the test can inject
     * the interface it wants and mock the data access layer.
     *
     * @package Demo\Rpc
     */
    class DependencyInjectedRpc {
        private SomeDataAccessInterface $someData;

        /**
         * DependencyInjectedRpc constructor.
         *
         * @param SomeDataAccessInterface $someData
         */
        public function __construct(SomeDataAccessInterface $someData) {
            $this->someData = $someData;
        }


        function getPersonInfo(PersonInfoRequest $request): PersonInfoResponse {
            $response = new PersonInfoResponse($request->personId, 0);
            $response->age = $this->someData->getPersonAge($request->personId);
            return $response;
        }

        static function initFromRequest(ServerRequestInterface $request): DependencyInjectedRpc {
            $someData = new SomeDataAccess(); // Create the default instance of the SomeDataAccessInterface dependency
            return new DependencyInjectedRpc($someData);
        }
    }