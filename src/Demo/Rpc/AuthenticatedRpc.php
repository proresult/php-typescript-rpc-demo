<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Rpc;

    use DateTimeImmutable;
    use Demo\Models\HelloResponse;
    use Demo\Models\Session;
    use Demo\RequestTypes\SessionRequest;
    use Demo\AuthControl\SessionAuthController;
    use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
    use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
    use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

    #[RequestType(SessionRequest::class)]
    #[AuthType(SessionAuthController::class, "default")]
    class AuthenticatedRpc {
        public const ROLE_ADMIN = "Admin";

        public function __construct(private Session $session) {
        }

        public static function initFromRequest(SessionRequest $request): AuthenticatedRpc {
            return new AuthenticatedRpc($request->session());
        }


        #[Get]
        public function activeSession(): Session {
            return $this->session;
        }

        #[Get]
        #[AuthCheck(SessionAuthController::ROLE_ADMIN)]
        public function adminOnly(): HelloResponse {
            $tomorrow = new RpcDateTime(new DateTimeImmutable("tomorrow"));
            return new HelloResponse("Only admin knows this", $tomorrow);
        }
    }