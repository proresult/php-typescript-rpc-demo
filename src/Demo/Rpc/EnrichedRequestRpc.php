<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Rpc;

    use Demo\Models\ClientVersionResponse;
    use Demo\RequestTypes\EnrichedRequest;
    use Demo\Models\RequestIdResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
    use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;

    #[RequestType(EnrichedRequest::class)]
    class EnrichedRequestRpc {

        public function __construct(private int $requestId, private ?string $clientVersion) {}

        static function initFromRequest(EnrichedRequest $request): EnrichedRequestRpc {
            return new EnrichedRequestRpc($request->getRequestId(), $request->getClientVersion());
        }

        #[Get]
        public function requestId(): RequestIdResponse {
            return new RequestIdResponse($this->requestId);
        }

        #[Get]
        public function clientVersion(): ClientVersionResponse {
            return new ClientVersionResponse($this->clientVersion);
        }
    }