<?php
    declare(strict_types=1);

    namespace Demo\Rpc;


    /* This tries to break the codegen with more difficult kinds of rpcs */

    use Demo\Models\ComplexEntity;
    use Demo\Models\Sub\SubEntity1;
    use Psr\Http\Message\ServerRequestInterface;

    class ComplexRpc {
        public function echoComplex(ComplexEntity $request): ComplexEntity {
            $request->counter = $request->counter + 1;
            if($request->counter % 3 == 0) {
                $request->numOrNull = null;
                $request->numOrText = 2134;
                $request->entity1OrNull = null;
            } else {
                $request->numOrNull = 999;
                $request->numOrText = "abadsbab";
                $request->entity1OrNull = new SubEntity1("entity 1 was null, But Not anymore!");
            }
            return $request;
        }

        static function initFromRequest(ServerRequestInterface $request): ComplexRpc {
            return new ComplexRpc();
        }
    }