<?php
    declare(strict_types=1);

    namespace Demo;


    use Laminas\Diactoros\Response;
    use Proresult\PhpTypescriptRpc\Server\ResponseFactoryInterface;
    use Psr\Http\Message\ResponseInterface;

    class ResponseFactory implements ResponseFactoryInterface {

        function newResponse(int $statusCode, ?string $body, ?string $contentType) : ResponseInterface {
            $response = new Response(status: $statusCode);
            if($body != null) {
                $response->getBody()->write($body);
                $response->getBody()->rewind();
            }
            if($contentType != null) {
                $response = $response->withHeader("Content-Type", $contentType);
            }
            return $response;
        }
    }