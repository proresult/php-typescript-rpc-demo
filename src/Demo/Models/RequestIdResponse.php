<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Models;

    class RequestIdResponse {
        public function __construct(public int $requestId) {}
    }