<?php
    declare(strict_types=1);

    namespace Demo\Models;


    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

    class HelloRequest {
        public function __construct(
            public string $name,
            public RpcDateTime $today,
        ) {}
    }