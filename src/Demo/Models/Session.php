<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Models;

    use Psr\Http\Message\ServerRequestInterface;

    class Session {
        public const SECRET_FIRST = "secret-first";
        public const SECRET_SECOND = "secret-second";
        public const SECRET_THIRD = "secret-third";

        public function __construct(public string $userId, public bool $isAdmin) {
        }

        public static function fromAuthHeader(string $secret): ?Session {
            // In real life code we would do some lookup in db, etc.
            return match($secret) {
                self::SECRET_FIRST => new Session("beta", false),
                self::SECRET_SECOND => new Session("delta", false),
                self::SECRET_THIRD => new Session("omikron", true),
                default => null,
            };
        }
    }