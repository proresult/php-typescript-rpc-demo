<?php

    /*
    Copyright 2022, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Models;

    class Contants {
        public const CONSTANT_TEXT = "abc";
        public const CONSTANT_NUM = 243;
    }