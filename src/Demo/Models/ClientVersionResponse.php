<?php

    /*
    Copyright 2022, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\Models;

    class ClientVersionResponse {
        public function __construct(public string | null $version) {
        }
    }