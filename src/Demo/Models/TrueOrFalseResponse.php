<?php
    declare(strict_types=1);

    namespace Demo\Models;

    class TrueOrFalseResponse {
        public bool $answer;
        public float $doubledBonus;

        /**
         * @param bool  $answer
         * @param float $doubledBonus
         */
        public function __construct(bool $answer, float $doubledBonus) {
            $this->answer       = $answer;
            $this->doubledBonus = $doubledBonus;
        }


    }