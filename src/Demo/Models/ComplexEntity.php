<?php
    declare(strict_types=1);

    namespace Demo\Models;


    use Demo\Models\Sub\SubEntity1;

    /**
     * Class ComplexEntity A entity demoing the more advanced type declarations that should work with php typescript rpc codegen.
     *
     * The generated typescript uses the number type for int and float php types.
     *
     * NB: Arrays must currently be declared as shown here, with type definition for both index and value. The more usual way to declare a numeric indexed array (e.g. string[]) is
     * not currently supported. Also, since php don't natively support defining the type of arrays, there must always be a docblock declaring it, like these examples show.
     *
     * Also note that it is recommended, if possible, to have a default initial value for all properties so that a default initialization without values is a valid object instance.
     * This is for forward/backwards compatibility, so that a future or old client don't get BadRequestException because the request is missing a property that is declared in the
     * request class. Ofcourse, the rpc method being called must also properly handle the default value of the property then.
     *
     * @param array<int, int> $numbersArr
     * @param array<string, int> $assocNums
     * @package Demo\Models
     */
    class ComplexEntity {
        public function __construct(
            public int $counter,
            public int | string $numOrText,
            public int | null $numOrNull,
            public int | string | null $numOrTextOrNull,
            public SubEntity1 $entity1,
            public ?SubEntity1 $maybeEntity1,
            /** @var array<int, int> $numbersArr */
            public array $numbersArr = [],
            /** @var array<string, int> $assocNums */
            public array $assocNums = [],
            public SubEntity1 | null $entity1OrNull = null,
        ) {}
    }