<?php
    declare(strict_types=1);

    namespace Demo\Models;


    class PersonInfoRequest {
        public int $personId;

        /**
         * PersonInfoRequest constructor.
         *
         * @param int $personId
         */
        public function __construct(int $personId) {
            $this->personId = $personId;
        }


    }