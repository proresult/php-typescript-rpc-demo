<?php
    declare(strict_types=1);

    namespace Demo\Models\Sub\Sub2;


    class SubEntity2 {
        public string $subText2;

        /**
         * SubEntity2 constructor.
         *
         * @param string $subText2
         */
        public function __construct(string $subText2) {
            $this->subText2 = $subText2;
        }


    }