<?php
    declare(strict_types=1);

    namespace Demo\Models\Sub;


    use Demo\Models\Sub\Sub2\SubEntity2;

    class SubEntity1 {
        public string $subText1;
        public ?SubEntity2 $maybeSub2 = null; // We must initialize this, otherwise the symfony deserializer fails it seems.

        /**
         * SubEntity1 constructor.
         *
         * @param string $subText1
         */
        public function __construct(string $subText1) {
            $this->subText1 = $subText1;
        }


    }