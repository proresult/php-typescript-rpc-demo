<?php
    declare(strict_types=1);

    namespace Demo\Models;


    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

    class HelloResponse {
        public function __construct(
            public string $message,
            public RpcDateTime $tomorrow,
        ) {}
    }