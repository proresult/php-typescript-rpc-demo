<?php
    declare(strict_types=1);

    namespace Demo\Models;


    class PersonInfoResponse {
        public function __construct(
            public int $personId,
            public int $age,
        ) {}
    }