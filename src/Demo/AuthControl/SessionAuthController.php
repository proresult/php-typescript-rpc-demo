<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\AuthControl;

    use Demo\Models\Session;
    use Demo\RequestTypes\SessionGetterInterface;

    /**
     * An auth controller that gets a ready made request class with a session-getter passed to it in the constructor.
     * Therefore requires the use of #RequestType on each rpc that uses it, to prepare the request object it gets.
     */
    class SessionAuthController {
        private Session $session;

        public const ROLE_ADMIN = "admin";

        public function __construct(SessionGetterInterface $sessionGetter) {
            $this->session = $sessionGetter->session(); // Throws UnauthorizedException if session is not authenticated
        }

        public function default(): bool {
            return true;
        }

        public function admin(): bool {
            return $this->session->isAdmin;
        }
    }