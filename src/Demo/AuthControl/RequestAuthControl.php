<?php

    /*
    Copyright 2021, Proresult AS (proresult.no).
    License: Proprietary.
    */
    declare(strict_types=1);

    namespace Demo\AuthControl;

    use Demo\Models\Session;
    use Demo\RequestTypes\EnrichedRequest;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
    use Psr\Http\Message\ServerRequestInterface;

    class RequestAuthControl {

        private Session $session;

        public function __construct(ServerRequestInterface $request) {
            // The passed request is the raw interface, not preprocessed with #RequestType attribute, so must extract
            // needed info from request from scratch.
            $sessionSecret = EnrichedRequest::sessionSecretFromHeader($request);
            $session = Session::fromAuthHeader($sessionSecret);
            if ($session !== null) {
                $this->session = $session;
            } else {
                throw new UnauthorizedException(UnauthorizedException::DEFAULT_MESSAGE);
            }
        }

        public function hello(string $name): bool {
            return mb_substr(mb_strtolower($name), 0, 1) === "j";
        }
    }