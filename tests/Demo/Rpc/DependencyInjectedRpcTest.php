<?php
    declare(strict_types=1);

    namespace Demo\Rpc;


    use Demo\Dal\SomeDataAccessFaked;
    use Demo\Models\PersonInfoRequest;
    use PHPUnit\Framework\TestCase;

    class DependencyInjectedRpcTest extends TestCase {

        /**
         * demoes a unit test where we inject a special test instance in testing, replacing the default prod instance.
         */
        function testGetPersonInfo() {
            // A map of person id to expected returned age
            $successTestCases = [
                1 => 34,
                2 => 26,
            ];

            $dependencyInjectedRpc = new DependencyInjectedRpc(new SomeDataAccessFaked());

            $successTestCaseRunner = function(int $personId, int $expectedAge) use($dependencyInjectedRpc) {
                $request = new PersonInfoRequest($personId);
                $response = $dependencyInjectedRpc->getPersonInfo($request);
                $this->assertEquals($expectedAge, $response->age);
                $this->assertEquals($personId, $response->personId);
            };
            foreach($successTestCases as $personId => $expectedAge) {
                $successTestCaseRunner($personId, $expectedAge);
            }
        }
    }