<?php
    declare(strict_types=1);

    namespace Demo\Rpc;


    use DateInterval;
    use DateTimeImmutable;
    use Demo\Models\HelloRequest;
    use Demo\Models\HelloResponse;
    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

    /**
     * Class HelloEndpointTest Tests the simple HelloEndpoint rpc class.
     *
     * @package Demo\Rpc
     */
    class HelloEndpointTest extends TestCase {

        function testHello() {
            $now = new DateTimeImmutable("2021-06-21 10:11:12");
            $request = new HelloRequest(
                "Clark \"Superman\" Kent",
                new RpcDateTime($now),
            );
            $helloEndpoint = new HelloEndpoint($now);
            $expectedResponse = new HelloResponse(
                "Hello, {$request->name}!",
                $request->today->add(new DateInterval("P1D")),
            );
            $actualResponse = $helloEndpoint->hello($request);

            $this->assertEquals($expectedResponse, $actualResponse);
        }
    }