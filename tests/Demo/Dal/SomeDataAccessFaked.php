<?php
    declare(strict_types=1);

    namespace Demo\Dal;


    /**
     * Class SomeDataAccessFaked Used in unit testing to mock the default "real" data access layer.
     *
     * @package Demo\Dal
     */
    class SomeDataAccessFaked implements SomeDataAccessInterface {
        function getPersonAge(int $personId) : int {
            return match($personId) {
                1 => 34,
                2 => 26,
                default => 0
            };
        }
    }