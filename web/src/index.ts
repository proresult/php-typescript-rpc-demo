import {HelloRequest} from "@/generated/rpc/Models/HelloRequest";
import {Client} from '@proresult/php-typescript-rpc';
import {failingHello, hello} from "@/generated/rpc/Rpc/HelloEndpoint";
import {ComplexEntity} from "@/generated/rpc/Models/ComplexEntity";
import {echoComplex} from "@/generated/rpc/Rpc/ComplexRpc";
import {SubEntity2} from "@/generated/rpc/Models/Sub/Sub2/SubEntity2";
import {SubEntity1} from "@/generated/rpc/Models/Sub/SubEntity1";
import {getPersonInfo} from "@/generated/rpc/Rpc/DependencyInjectedRpc";
import UserfriendlyException from "@proresult/php-typescript-rpc/dist/errors/UserfriendlyException";
import {getCircularRef, getSome, nullableArguments, trueOrFalse} from "@/generated/rpc/Rpc/GetRpc";
import {clientVersion, requestId} from "@/generated/rpc/Rpc/EnrichedRequestRpc";
import {activeSession} from "@/generated/rpc/Rpc/AuthenticatedRpc";
import ErrorResponse from "@proresult/php-typescript-rpc/dist/errors/ErrorResponse";
import {hello as authenticatedHello} from "@/generated/rpc/Rpc/OtherAuthenticatedRpc";
import {CircularRef} from "@/generated/rpc/Models/CircularRef";
import {CONSTANT_NUM, CONSTANT_TEXT} from "@/generated/rpc/Models/Contants";
import { version } from "@proresult/php-typescript-rpc/package.json";


const helloDemoSetup = (client: Client) => {
    const nameTxt = document.getElementById("nameTxt") as HTMLInputElement;
    const helloBtn = document.getElementById("helloBtn") as HTMLButtonElement;
    const responseTxt = document.getElementById("responseTxt") as HTMLSpanElement;
    const serverStartTxt = document.getElementById("serverStartTxt") as HTMLElement;


    const onHelloBtnClick = async () => {
        const name = nameTxt.value;
        const helloRequest: HelloRequest = {
            name: name,
            today: new Date(),
        };
        const helloResponse = await client.do(hello(helloRequest));
        responseTxt.textContent = helloResponse.message;
        serverStartTxt.textContent = "tomorrow is: " + helloResponse.tomorrow.toLocaleDateString();
    };

    helloBtn.addEventListener("click", onHelloBtnClick);
}

const complexDemoSetup = (client: Client) => {
    const complexBtn = document.getElementById("complexBtn") as HTMLButtonElement;
    const complexTxt = document.getElementById("complexTxt") as HTMLSpanElement;

    let complexEntity: ComplexEntity = {
        counter: 0,
        numOrNull: 123,
        numOrText: "textNotNum",
        numOrTextOrNull: null,
        entity1: {
            subText1: "sub text 1",
            maybeSub2: null,
        },
        maybeEntity1: null,
        numbersArr: [2, 4, 6],
        assocNums: {"a": 10, "b": 20},
        entity1OrNull: {
            subText1: "entity1 is NOT null!",
            maybeSub2: null,
        }
    }

    // Helpers to create string representation of a ComplexEntity
    const subEntity2Str = (se: SubEntity2 | null): string => se == null ? "NULL" : `subText2: ${se.subText2}`;
    const subEntity1Str = (se: SubEntity1 | null): string => se == null ? "NULL" : `subText1: ${se.subText1}, ${subEntity2Str(se.maybeSub2)}`
    const complextEntityStr = (e: ComplexEntity): string => `counter: ${e.counter} --- numOrNull: ${e.numOrNull} -- numOrTextOrNull: ${e.numOrTextOrNull} -- numOrText: ${e.numOrText} -- subEntity1: ${subEntity1Str(e.entity1)} -- maybeEntity1: ${subEntity1Str(e.maybeEntity1)} -- numbers: ${e.numbersArr} -- assocNums: ${JSON.stringify(e.assocNums)} -- entity1OrNull: ${subEntity1Str(e.entity1OrNull)}`;

    const onComplexBtnClick = async () => {
        console.debug(`Sending complex to server`, complexEntity);
        complexEntity = await client.do(echoComplex(complexEntity));
        console.debug(`Got complex from server`, complexEntity);
        complexTxt.textContent = complextEntityStr(complexEntity);
    }

    complexTxt.textContent = complextEntityStr(complexEntity);

    complexBtn.addEventListener("click", onComplexBtnClick);
}

const dependencyInjectedRpcDemoSetup = (client: Client) => {
    const personIdTxt = document.getElementById("personIdTxt") as HTMLInputElement;
    const getPersonInfoBtn = document.getElementById("getPersonInfoBtn") as HTMLButtonElement;
    const personInfoAge = document.getElementById("personInfoAge") as HTMLSpanElement;

    const onGetPersonInfoBtnClick = async () => {
        const personId = Number(personIdTxt.value);
        const info = await client.do(getPersonInfo({personId}));
        personInfoAge.textContent = ""+info.age;
    }
    getPersonInfoBtn.addEventListener("click", onGetPersonInfoBtnClick);
}

const failingHelloSetup = (client: Client) => {
    const failingResponse = document.getElementById("failingResponse") as HTMLDivElement;
    const failingHelloBtn = document.getElementById("failingHelloBtn") as HTMLButtonElement;
    const onFailingHelloBtnClick = async () => {
        const helloRequest: HelloRequest = {
            name: "Fail Ure",
            today: new Date(),
        };
        try {
            // The failingHello rpc throws a UserfriendlyException, so the client will also throw a UserfriendlyException that we can handle.
            const helloResponse = await client.do(failingHello(helloRequest));
            throw new Error("Did not get expected error from failingHello request");
        } catch (err) { // Should get a UserfriendlyException here
            if(err instanceof UserfriendlyException) {
                failingResponse.textContent = `Code: ${err.code} --- Message: ${err.message}`;
            } else {
                throw err;
            }
        }
    }
    failingHelloBtn.addEventListener("click", onFailingHelloBtnClick);
}

const getSomeSetup = (client: Client) => {
    const txt1Txt = document.getElementById("txt1Txt") as HTMLInputElement;
    const txt2Txt = document.getElementById("txt2Txt") as HTMLInputElement;
    const num1Txt = document.getElementById("num1Txt") as HTMLInputElement;
    const getSomeBtn = document.getElementById("getSomeBtn") as HTMLButtonElement;
    const getSomeBack = document.getElementById("getSomeBack") as HTMLSpanElement;

    const onGetSomeBtnClick = async () => {
        const txt1 = txt1Txt.value;
        const txt2 = txt2Txt.value.length > 0 ? txt2Txt.value : null;
        const num1 = Number(num1Txt.value);

        const getSomeResponse = await client.do(getSome(txt1, num1, txt2));
        getSomeBack.textContent = `txt1: ${getSomeResponse.txt1}, num1: ${getSomeResponse.num1}, txt2: ${getSomeResponse.txt2}`;
    }
    getSomeBtn.addEventListener("click", onGetSomeBtnClick);
}

const trueOrFalseSetup = (client: Client) => {
    const trueOrFalseCheck = document.getElementById("trueOrFalseCheck") as HTMLInputElement;
    const bonusTxt = document.getElementById("bonusTxt") as HTMLInputElement;
    const trueOrFalseBtn = document.getElementById("trueOrFalseBtn") as HTMLButtonElement;
    const trueOrFalseResult = document.getElementById("trueOrFalseResult") as HTMLSpanElement;

    const onTrueOrFalseClick = async () => {
        const isTrue = trueOrFalseCheck.checked;
        const bonus = Number(bonusTxt.value);
        const result = await client.do(trueOrFalse(isTrue, bonus));
        trueOrFalseResult.textContent = `true? ${result.answer ? "YES" : "NO"}. Bonus: ${result.doubledBonus}`;
    }
    trueOrFalseBtn.addEventListener("click", onTrueOrFalseClick);
}

const nullableArgumentsSetup = (client: Client) => {
    const txt1Txt = document.getElementById("nullableTxt1Txt") as HTMLInputElement;
    const txt2Txt = document.getElementById("nullableTxt2Txt") as HTMLInputElement;
    const num1Txt = document.getElementById("nullableNum1Txt") as HTMLInputElement;
    const nullableArgumentsBtn = document.getElementById("nullableArgumentsBtn") as HTMLButtonElement;
    const nullableArgumentsBack = document.getElementById("nullableArgumentsBack") as HTMLSpanElement;

    const onNullableArgumentsBtnClick = async () => {
        const txt1 = txt1Txt.value.length > 0 ? txt1Txt.value : null;
        const num1 = num1Txt.value.length > 0 ? Number(num1Txt.value) : null;
        const txt2 = txt2Txt.value.length > 0 ? txt2Txt.value : null;

        const getSomeResponse = await client.do(nullableArguments(txt1, num1, txt2));
        nullableArgumentsBack.textContent = `txt1: ${getSomeResponse.txt1}, num1: ${getSomeResponse.num1}, txt2: ${getSomeResponse.txt2}`;
    }
    nullableArgumentsBtn.addEventListener("click", onNullableArgumentsBtnClick);
}

const getCircularRefSetup = (client: Client) => {
    const getCircularRefBtn = document.getElementById("getCircularRefBtn") as HTMLButtonElement;
    const circularRefReturned = document.getElementById("circularRefReturned") as HTMLSpanElement;

    const getCircular = async () => {
        const response = await client.do(getCircularRef());
        const circularPrint = (circular: CircularRef): string =>
            (circular.innerRef1 ? `inner: ${ circularPrint(circular.innerRef1)} -/- ` : ``) +
            ` someVal: ${circular.someValue}` +
            (circular.innerRefs.length > 0 ? ` |||| ${circular.innerRefs.map(circularPrint)}` : ``);
        circularRefReturned.textContent = circularPrint(response)
    }
    getCircularRefBtn.addEventListener("click", getCircular);
}

const enrichedSetup = (client: Client) => {
    const requestIdBtn = document.getElementById("requestIdBtn") as HTMLButtonElement;
    const requestIdTxt = document.getElementById("requestIdTxt") as HTMLSpanElement;
    const requestIdInp = "991122";
    client.requestHeaders.set("X-Request-Id", requestIdInp);
    const onRequestIdClick = async () => {
        const result = await client.do(requestId());
        requestIdTxt.textContent = "" + result.requestId;
    }
    requestIdBtn.addEventListener("click", onRequestIdClick);
}

const isUnauthorized = (e: Error) => e instanceof ErrorResponse && e.status === 401;
const isForbidden = (e: Error) => e instanceof ErrorResponse && e.status === 403;

const authenticatedSetup = (client: Client) => {
    const sessionTokenInp = document.getElementById("sessionTokenInp") as HTMLInputElement;
    const sessionLoginBtn = document.getElementById("sessionLoginBtn") as HTMLButtonElement;
    const activeSessionTxt = document.getElementById("activeSessionTxt") as HTMLSpanElement;
    const helloInp = document.getElementById("authenticatedHelloNameInp") as HTMLInputElement;
    const helloBtn = document.getElementById("authenticatedHelloBtn") as HTMLButtonElement;
    const helloReplyTxt = document.getElementById("authenticatedHelloReplyTxt") as HTMLSpanElement;
    const errorTxt = document.getElementById("errorMessageTxt") as HTMLSpanElement;

    const errorHandler = (e: Error) => {
        if(isUnauthorized(e)) {
            errorTxt.textContent = `Login failed, invalid credentials`;
            return; // EXIT
        } else if(isForbidden(e)) {
            errorTxt.textContent = `User not allowed to do that`;
            return; // EXIT
        } else {
            errorTxt.textContent = ``;
        }
        // Unhandled errors get thrown again
        throw e;
    }

    const onSessionLoginBtnClick = async () => {
        errorTxt.textContent = ``;
        client.requestHeaders.set("X-Auth-Secret", sessionTokenInp.value);
        try {
            const result = await client.do(activeSession());
            activeSessionTxt.textContent = `${result.userId} --- ${result.isAdmin ? '(Admin)' : '(Normal)'}`
        } catch (e) {
            errorHandler(e);
        }
    }
    const onHelloBtnClick = async () => {
        errorTxt.textContent = ``;
        client.requestHeaders.set("X-Auth-Secret", sessionTokenInp.value);
        try {
            const result = await client.do(authenticatedHello({name: helloInp.value, today: new Date()}));
            helloReplyTxt.textContent = result.message;
            console.debug(`Tomorrow is ${result.tomorrow}`);
        } catch (e) {
            errorHandler(e);
        }
    }
    sessionLoginBtn.addEventListener("click", onSessionLoginBtnClick);
    helloBtn.addEventListener("click", onHelloBtnClick);
}

const constantsPrint = () => {
    const makeLi = (txt: string) => {
        const li = document.createElement("li");
        li.innerText = txt;
        return li;
    }
    const ul = document.getElementById("constantsUl");
    ul?.append(makeLi(CONSTANT_TEXT));
    ul?.append(makeLi(`${CONSTANT_NUM}`));
}

const clientVersionSetup = async (client: Client) => {
    const btn = document.getElementById("clientVersionBtn") as HTMLButtonElement;
    const builtTxt = document.getElementById("clientVersionBuiltTxt") as HTMLSpanElement;
    const returnedTxt = document.getElementById("clientVersionReturnedTxt") as HTMLSpanElement;

    builtTxt.textContent = version;

    const doRoundtrip = async () => {
        const resp = await client.do(clientVersion());
        returnedTxt.textContent = resp.version;
    }
    btn.addEventListener("click", doRoundtrip);
}

const main = () => {
    const client = new Client("localhost", 8040, "/rpc");
    helloDemoSetup(client);
    complexDemoSetup(client);
    dependencyInjectedRpcDemoSetup(client);
    failingHelloSetup(client);
    getSomeSetup(client);
    nullableArgumentsSetup(client);
    getCircularRefSetup(client);
    trueOrFalseSetup(client);
    enrichedSetup(client);
    authenticatedSetup(client);
    constantsPrint();
    clientVersionSetup(client);
}

// Since this is loaded as a javascript module, it is loaded deferred, meaning this main call always runs after the DOM is loaded.
// So this should be safe to do without doing a onDomContentLoaded event listen.
main();