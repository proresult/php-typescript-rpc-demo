Proresult Php Typescript Rpc Demo
=================================

This is a demo project showing how to use [proresult/php-typescript-rpc-codegen](https://bitbucket.org/proresult/php-typescript-rpc-codegen).

Development setup
-----------------

### Prerequisites

- A local install of php 8.

- A local install of node js.

### Project info

- The __src/Demo__ directory has the php "input" code for the generator and a example index.php implementing the request+response handling.

- Look at the __build/RpcCodeGenerator.php__ file to see how to setup and run the code generator.

- The __web/__ directory has the example code for the web client using the generated typescript to do rpc requests.

- Look at the __web/package.json__ file to see required web dependencies.

- Look at the __web/tsconfig.json__ file to see the required typescript config. _("paths" @generated mapping)_

- Look at the __composer.json__ file to see the required php dependencies.

Development workflow
--------------------

1. Craft php model and rpc classes in the defined input directories **(src/Demo/Models, src/Demo/Rpc)**.

2. Create server side unit tests in tests/Demo, if applicable.
   
3. Run the code generator with the composer script **"rpc:generate"**.

4. If a new Rpc class was added, hook it up to RpcRouter in _src/index.php_

5. Edit the client side web code to use the generated typescript rpc code.

6. Compile the typescript with the `npm run compile` command.

7. Run the server with composer script **"rpc:serve"**. This will start a php cli server on **localhost:8040**.

8. Run the web client development compiler and server with `npm run dev` with your terminal in the _web/_ directory. This will start a esbuild development server on **localhost:8000**

9. Open your browser at localhost:8080 to try the demo.

10. Edit and adjust, repeat codegen if you change any php input code. The webserver automatically recompiles the client side typescript if you make changes there.