<?php
    declare(strict_types=1);

    namespace Build;


    use Proresult\PhpTypescriptRpc\Codegen\Codegen;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePath;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustment;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Name;

    class RpcCodegenerator {
        static function run() {
            $modelsDir = __DIR__ . '/../src/Demo/Models';
            $rpcDir    = __DIR__ . '/../src/Demo/Rpc';
            $tsOutDir  = __DIR__ . '/../web/src/generated';
            $phpOutDir = __DIR__ . '/../src/Demo/Adapters';

            $localPathAdjust = new ModulePathPrefixAdjustment(
                remove: new ModulePath(Name::arrayFromStrings("Demo")),
                add: new ModulePath(Name::arrayFromStrings("rpc")),
            );
            $rpcDateTimePathAdjust = new ModulePathPrefixAdjustment(
                new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Server", "Models", "RpcDateTime")),
                new ModulePath(Name::arrayFromStrings("@proresult", "php-typescript-rpc"), isGenerated: false),
            );
            $pathAdjust = new ModulePathPrefixAdjustments($localPathAdjust, $rpcDateTimePathAdjust);

            $baseRpcPhpNamespace = new NamespaceRep(["Demo", "Rpc"]);

            $phpNamespaceAdjuster = new NamespaceAdjuster(remove: "\\Rpc", add: "\\Adapters");

            $codeGenerator = new CodeGen(
                modelInputBaseDir: $modelsDir,
                rpcInputBaseDir: $rpcDir,
                typescriptOutputDir: $tsOutDir,
                phpAdapterOutputDir: $phpOutDir,
                pathPrefixAdjustment: $pathAdjust,
                baseRpcPhpNamespace: $baseRpcPhpNamespace,
                phpNamespaceAdjuster: $phpNamespaceAdjuster,
                typescriptHeaderContent: "/* eslint-disable */".PHP_EOL,
            );

            $codeGenerator->run();
        }
    }
